import { useState, useEffect } from 'react';


//can create custom components like Person
//props are arguments passed into react components
//props allow you to pass dynamic data thru
//state is an object used to take note of the current situation of another object
// use is a hook
//useState(initialState) intial state can be value of some sort
//const [counter, setCounter] first is name of the param and the second is the setter function
//alert() to do alert popup
//can create callback functions in onClick = {() => function} and setter functions as well
//useEffect hook allows you to do something on event occur
//never modify state manually only using its setter function
//second param to useEffect is the dependency array
//dependency array takes in params that the useEffect is should be reliant on
const Person = (props) => {
  return (
    <>
    <h1> Name: {props.name}</h1>
    <h2>Last Name: {props.lastName}</h2>
    <h2>Age: {props.age}</h2>
    </>
  )
}
const Practice = () => {
  const [counter, setCounter] = useState(0)


  useEffect(()=> {
    alert("You've changed the counter to " + counter);

  }, [counter]);
  //this is jsx
  return (
    <div className="Practice">
      <button onClick={() => setCounter((prevCount) => prevCount - 1)}> - </button>
      <h1> {counter} </h1>
      <button onClick={() => setCounter((prevCount) => prevCount + 1)}> + </button>
    </div>
  );
}

export default Practice;

// react fragments are required to render more than one line of html in jsx <> </>
// can use react fragments for boolean logic
