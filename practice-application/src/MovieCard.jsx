import React from "react";


//object destructure props so that you dont have to use it too many times


const MovieCard = ({ movieComponent }) => {
    return (
        <div className="movie">
                <div>
                    <p>
                        {movieComponent.Year}
                    </p>
                </div>
                <div>
                    <img src={movieComponent.Poster !== 'N/A' ? movieComponent.Poster : 'https://via.placeholder.com/400'} alt={movieComponent.Title}/>
                </div>

                <div>
                    <span>{movieComponent.Type}</span>
                    <h3>{movieComponent.Title}</h3>
                </div>
        </div>
    );
}


export default MovieCard;