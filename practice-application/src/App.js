import React from "react";
import { useEffect,useState } from "react";


//api key 4e6dc03c

import './App.css'
import SearchIcon from './search.svg'
import MovieCard from "./MovieCard.jsx"

const API_URL = 'http://www.omdbapi.com/?apikey=4e6dc03c'


const movieOne = {
    "Title": "Superman, Spiderman or Batman",
    "Year": "2011",
    "imdbID": "tt2084949",
    "Type": "movie",
    "Poster": "https://m.media-amazon.com/images/M/MV5BMjQ4MzcxNDU3N15BMl5BanBnXkFtZTgwOTE1MzMxNzE@._V1_SX300.jpg"
}
const App = () => {
    const [movies, setMovies] = useState([])
    const [searchTerm, setSearchTerm] = useState('')
    const searchMovies = async(title) => {
        const response = await fetch(`${API_URL}&s=${title}`);
        const data = await response.json();
        setMovies(data.Search);
    }

    useEffect(() => {
        searchMovies('Spiderman');
    }, []);

    return (
        <div className='App'>
            <h1>MovieLand</h1>

            <div className="search">
                <input 
                    placeholder = "Search for movies"
                    value = {searchTerm}
                    onChange={(event) => setSearchTerm(event.target.value)}
                />
                <img
                    src={SearchIcon}
                    alt = "search"
                    onClick={() => searchMovies(searchTerm)}
                    />
            </div>

            { movies?.length > 0
                ? (
                    <div className="container">
                        {movies.map((movie) => (
                            <MovieCard movieComponent={movie} />
                        ))
                        }
                    </div>
                ): (
                    <div className="empty">
                        <h2>
                            No Movies Found
                        </h2>
                    </div>
                )
            }      
        </div>
    );
}


export default App;

//the N/A in the poster script is relevant to the API not React itself.

//when something is repeated multiple times hsould make that into seperate component.