import React from "react";
import ReactDOM from 'react-dom'

import App from './App.js'

//render (component we want to render, get the element by id)
//this injects the entire react application into root
ReactDOM.render(<App />, document.getElementById('root'));

